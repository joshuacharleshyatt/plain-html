# Example HTML

This project uses Quarto to render a static site on Gitlab using Gitlab pages. From project main run
```console
quarto render
```

Site is generated in public folder and published utilizing gitlab pages
[website](https://joshuacharleshyatt.gitlab.io/plain-html/analysis.html)
